import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Ex2 extends JFrame {
        JTextField text1, text2;
        JButton button;
        JLabel label;

        Ex2() {
            setTitle("File content");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            set();
            setSize(500, 500);
            setVisible(true);
        }

        public void set() {
            this.setLayout(null);

            button = new JButton("Content!");
            button.setBounds(70, 300, 200, 50);

            text1 = new JTextField();
            text1.setBounds(100, 100, 200, 30);

            text2 = new JTextField();
            text2.setBounds(40, 150, 400, 120);

            label = new JLabel("File: ");
            label.setBounds(30, 100, 200, 30);

            button.addActionListener(new Action());

            add(button);
            add(text1);
            add(text2);
            add(label);
        }

        public static void main(String[] args) {

            Ex2 file = new Ex2();
        }

        public class Action implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String name = text1.getText();
                    BufferedReader reader = new BufferedReader(new FileReader(name));
                    int nr=0;
                    String line;
                    line = reader.readLine();
                    while (line != null) {
                        nr=nr+1;
                        line = reader.readLine();
                    }
                    text2.setText(String.valueOf(nr));
                } catch (Exception ex) {
                    text2.setText("File not found! ");
                }
            }
        }
    }
